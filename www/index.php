<?php
    error_reporting( E_ALL & ~E_WARNING & ~E_NOTICE );

    $templateUrl = 'test.html';
    $targetMarkup = '</head>';

    $fullPage = file_get_contents( $templateUrl );

    $fullPage = str_ireplace(
        $targetMarkup,

        '<script src="scripts/fbinhouse-injector.dev.js"></script>' . "\n" . $targetMarkup,
        $fullPage
    );

    echo $fullPage;

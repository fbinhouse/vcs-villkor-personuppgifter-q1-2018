( function(){
    'use strict';

    var scripts = document.getElementsByTagName( 'script' );
    var script = scripts[scripts.length - 1];

    var fbinhouse = {
        isIeDetected: false,

        isMobile: function(){
            if(( typeof window.matchMedia !== 'undefined' || typeof window.msMatchMedia !== 'undefined' ) && window.matchMedia( '( max-width: 480px )' ).matches ) {
                return true;
            }

            return false;
        },

        initResponsiveImages: function( $ ){
            $( 'img[data-responsive-img]' ).each( function(){
                $( this ).attr( 'src', fbinhouse.baseUrl + $( this ).data( 'responsive-img' ));
            });
        },

        isIpad: function(){
            return navigator.userAgent.match( /ipad/gim );
        },

        isIOS: function(){
            return navigator.userAgent.match( /(iPad|iPhone|iPod)/g );
        },

        isOldIE: function( $ ){
            if( !fbinhouse.isIeDetected ){
                fbinhouse.detectIe( $ );
            }

            return $( 'html' ).is( '.oldie' );
        },

        isIE: function( $, version ){
            if( !fbinhouse.isIeDetected ){
                fbinhouse.detectIe( $ );
            }

            return $( 'html' ).is( '.ie' + version );
        },

        detectIe: function( $ ){
            if( navigator && navigator.appName && navigator.appName.indexOf( 'Internet Explorer' ) > -1 ) {
                if( navigator.appVersion.indexOf( 'MSIE 9.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie9' );
                } else if( navigator.appVersion.indexOf( 'MSIE 8.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie8 oldie' );
                } else if( navigator.appVersion.indexOf( 'MSIE 7.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie7 oldie' );
                } else if( navigator.appVersion.indexOf( 'MSIE 6.' ) > 1 ) {
                    $( 'html' ).addClass( 'ie6 oldie' );
                }
            }

            fbinhouse.isIeDetected = true;
        },

        scriptUrl: script.src,

        getScriptParam: function( name ) {
            var n = name.replace( /[\[\]]/g, '\\$&' );
            var regex = new RegExp( '[?&]' + n + '(=([^&#]*)|&|#|$)' );
            var results = regex.exec( fbinhouse.scriptUrl );

            if ( !results ) {
                return 'villkor';
            }
            if ( !results[2] ){
                return 'villkor';
            }

            return decodeURIComponent( results[2].replace( /\+/g, '' ));
        }
    };

    window.fbinhouse = fbinhouse;

})();

fbinhouse.projectName = 'vcs-villkor-personuppgifter-q1-2018';

fbinhouse.client = {
    name : 'volvo',
    site : 'volvocars.com',
    host : 'vcs.fbinhouse.se'
};

if( window.location.href.indexOf( 'localhost' ) > -1 ){
    // Localhost
    fbinhouse.baseUrl = 'http://localhost:8888/' + fbinhouse.projectName + '/www/';
} else if( window.location.href.match( /10\.0\.\d{1,3}\.\d{1,3}/ )){
    // Preview on our internal IP adresses
    fbinhouse.baseUrl = 'http://' + window.location.host + '/' + fbinhouse.projectName + '/www/';
} else if( window.location.href.indexOf( 'hector.fbinhouse.se' ) > -1 ){
    // Dynamic match of devil versions
    if( window.location.href.indexOf( '/release/' ) > -1 ){
        // Dynamic match of release versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/release/' + window.location.href.match( /\d+\.\d+\.\d+/ )[ 0 ] + '/';
    } else if( window.location.href.indexOf( '/hotfix/' ) > -1 ){
        // Dynamic match of hotfix versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/hotfix/' + window.location.href.match( /\d+\.\d+\.\d+/ )[ 0 ] + '/';
    } else {
        // Match of feature versions
        fbinhouse.baseUrl = 'http://hector.fbinhouse.se/' + fbinhouse.projectName + '/feature/' + window.location.href.match( /feature\/(.+?)\// )[ 1 ] + '/';
    }
} else {
    fbinhouse.baseUrl = '//' + fbinhouse.client.host + '/' + fbinhouse.projectName + '/';
}

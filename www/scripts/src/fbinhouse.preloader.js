(function(){
    document.documentElement.style.overflow = 'scroll';

    var fadeOutDuration = 1;
    var style = document.createElement("style");

    var styleContent =
        '@keyframes removeoverlay {0% { opacity: 1;display: block;height: 100%;} 33% { opacity: 1;display: block;height: 100%;} 99% { opacity: 0;display: block;height: 100%;} 100% { opacity: 0;display: none;height: 0;}}' +
        '.__fbinhouse_spacer{height: 100%; width: 100%; background-color: #ffffff; position: fixed; top: 65px; left: 0; z-index: 998; display: flex; align-items: center; justify-content: center;}' +
        '.__fbinhouse_spacer.__fbinhouse_spacer_fadeout{-webkit-animation: removeoverlay 1000ms; -moz-animation: removeoverlay 1000ms; -o-animation: removeoverlay 1000ms; animation: removeoverlay 1000ms; opacity: 0; height: 0;}' +
        '.__fbinhouse_loader, .__fbinhouse_loader:before, .__fbinhouse_loader:after { border-radius: 50%; width: 2.5em; height: 2.5em; -webkit-animation-fill-mode: both; animation-fill-mode: both; -webkit-animation: load7 1.8s infinite ease-in-out; animation: load7 1.8s infinite ease-in-out; }' +
        '.__fbinhouse_loader { color: #212721; font-size: 1.1vh; margin: -80px auto 0; position: absolute; text-indent: -9999em; -webkit-transform: translateZ(0); -ms-transform: translateZ(0); transform: translateZ(0); -webkit-animation-delay: -0.16s; animation-delay: -0.16s; }' +
        '.__fbinhouse_loader.loaded { font-size: 1.15vh }' +
        '.__fbinhouse_loader:before, .__fbinhouse_loader:after { content: ""; position: absolute; top: 0; }' +
        '.__fbinhouse_loader:before { left: -3.5em; -webkit-animation-delay: -0.32s; animation-delay: -0.32s; }' +
        '.__fbinhouse_loader:after { left: 3.5em; }' +
        '@-webkit-keyframes load7 { 0%, 80%, 100% { box-shadow: 0 2.5em 0 -1.3em; } 40% { box-shadow: 0 2.5em 0 0; }}' +
        '@keyframes load7 { 0%, 80%, 100% { box-shadow: 0 2.5em 0 -1.3em; } 40% { box-shadow: 0 2.5em 0 0; }}'
    ;

    style.setAttribute("rel", "stylesheet");
    style.setAttribute("media", "screen");
    document.head.appendChild(style);

    if( !!( window.attachEvent && !window.opera )) style.styleSheet.cssText = styleContent;//this one's for ie
    else style.appendChild( document.createTextNode( styleContent ) );

    document.write('<div class="__fbinhouse_spacer"><div class="__fbinhouse_loader"></div></div>');
    document.querySelector( '.__fbinhouse_loader' ).className = '__fbinhouse_loader loaded';
    fbinhouse.preloader = {

        fadeOut: function(){
            //document.querySelector( '.__fbinhouse_loader' ).style.display = 'none';
            document.querySelector('.__fbinhouse_spacer').className = '__fbinhouse_spacer_fadeout __fbinhouse_spacer';
            setTimeout(function(){
                document.documentElement.style.overflow = 'auto';
            }, fadeOutDuration * 1000 );
        }

    };

    setTimeout(function(){
        fbinhouse.preloader.fadeOut();
    }, 7000);
})();

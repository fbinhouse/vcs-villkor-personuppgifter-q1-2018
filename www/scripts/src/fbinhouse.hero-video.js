( function( $ ){
    'use strict';
    fbinhouse.newHeroVideo = function( videoId ){
        var $playButtonEmbedTarget = $( 'h1' );
        var $videoEmbedTarget = $( '.fbi-headerblock' );
        var $embedCode = $( '<iframe width="100%" height="100%" src="" frameborder="0" allowfullscreen></iframe>' );
        var $playButton = $( '<a href="#" class="fbi-hero-video-play-btn">' );
        var $hideVideoButton = $( '<div>Stäng videon</div>' );

        $embedCode
            .css({
                position: 'absolute',
                left: 0,
                top: 0,
                zIndex: 50,
                width: '100%',
                height: '100%',
                display: 'none'
            });

        // Setup things for the play button
        $playButton
            .on( 'click touchstart', function( event ){
                event.preventDefault();

                $embedCode
                    .attr( 'src', 'https://www.youtube.com/embed/' + videoId + '?rel=0&ecver=2&cc_load_policy=1' )
                    .velocity( 'transition.fadeIn', {
                        duration: 400,
                        delay: 100,
                        ease: 'easeInSine'
                    });

                $hideVideoButton
                    .velocity( 'transition.slideDownIn' );
            });

        // Setup things for the hide video button
        $hideVideoButton
            .css({
                backgroundColor: '#000000',
                color: '#ffffff',
                display: 'none',
                fontFamily: '"Volvo Sans Medium", sans',
                height: '40px',
                left: '0',
                lineHeight: '40px',
                position: 'absolute',
                right: '0',
                textAlign: 'center',
                zIndex: 30
            })
            .hover( function(){
                $( this ).css({
                    cursor: 'pointer'
                });
            })
            .on( 'click touchstart', function( event ){
                event.preventDefault();

                $hideVideoButton
                    .velocity( 'transition.slideUpOut' );

                $embedCode.velocity( 'transition.fadeOut', {
                    duration: 300,
                    ease: 'easeOutSine',
                    complete : function(){
                        $embedCode.attr( 'src', function( i, val ){
                            return val;
                        });
                    }
                });
            });

        // Add the play button
        $playButtonEmbedTarget.append( $playButton );

        // Add the video and the hide video button
        $videoEmbedTarget
            .append( $embedCode )
            .after( $hideVideoButton );

    }
})( jQuery );

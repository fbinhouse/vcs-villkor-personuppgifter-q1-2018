fbinhouse.init = function( $ ){
    'use strict';

    fbinhouse.detectIe( $ );

    $( 'html' ).addClass( 'loaded' );

    fbinhouse.initLargeImages();
    fbinhouse.initGalleryGrid( '.gallery-grid' );
    fbinhouse.callbacks.initCallbacks();

    $( '.fbi-headerblock' ).imagesLoaded()
        .done( function(){
            fbinhouse.preloader.fadeOut();
        });

        //var site = fbinhouse.getScriptParam( 'site' );

    $( '#fbi-content' ).imagesLoaded()
        .done( function( elem ){
            fbinhouse.resizeLargeImages();
        });

        var type = window.location.hash.substr(1);

        if(type == 'villkor') {
            $('.villkor').show();
            $('.personuppgifter').hide();
        } else {
            $('.villkor').hide();
            $('.personuppgifter').show();
        }

};

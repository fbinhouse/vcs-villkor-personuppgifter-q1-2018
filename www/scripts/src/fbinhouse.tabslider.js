( function( $ ){
    'use strict';

    function TabSlider( selector ){
        var currentSlider = this;
        var $tabsContainer = $( selector );
        var items = $tabsContainer.find( '.tab-content' );
        var currentTabIndex = 1;
        var touch = false;
        var events = {};
        var start = {
            x : 0,
            y : 0
        };
        var leftDistance = 125;
        var swipeLengthThresholdX = 60;
        var swipeLengthThresholdY = 40;

        // if user accidentally omits the new keyword, this will
        // silently correct the problem...
        if( !( this instanceof TabSlider )){
            return new TabSlider( selector );
        }

        currentSlider.delta = {
            x : 0,
            y : 0
        };

        this.slideTo = function( index ){
            var animation = {
                opacity: 1,
                left: [ 0, ( index > currentTabIndex ? 1 : -1 ) * leftDistance ]
            };
            var options = {
                duration: 300,
                queue: false,
                easing: 'easeOutCubic'
            };

            // Don't do anything if we don't switch indexes
            if( index === currentTabIndex ){
                return false;
            }

            currentTabIndex = index;

            $tabsContainer.find( '.tab-content:visible' ).velocity({
                opacity: 0
            }, options );

            $tabsContainer.find( '.tab-content[data-index="' + index + '"]' ).velocity( animation, options );

            $tabsContainer.find( '.tab-content[data-index="' + index + '"]' ).filter( '.active' ).removeClass( 'active' ).end().filter( '[data-index="' + index + '"]' ).addClass( 'active' );

            $tabsContainer.find( '.tab' ).filter( '.active' ).removeClass( 'active' ).end().filter( '[data-index="' + index + '"]' ).addClass( 'active' );

            return true;
        };

        this.bindEvents = function(){
            var _this = this;

            $tabsContainer.on( events.down, _this.startEvent );

            $( '.tabs', $tabsContainer ).on( 'click', '.tab', function( event ){
                event.preventDefault();
                _this.slideTo( $( this ).data( 'index' ));
            });
        };

        this.move = function( basicEvent ){
            var event;
            var delta = {
                x : 0,
                y : 0
            };
            var decided;

            if( touch ) {
                event = basicEvent.originalEvent.changedTouches[ 0 ];
            } else {
                if( window.navigator.msPointerEnabled ) {
                    event = basicEvent.originalEvent;
                } else {
                    event = basicEvent;
                }
            }

            delta.x = event.pageX - start.x;
            delta.y = event.pageY - start.y;

            //console.log( delta );
            currentSlider.delta = delta;

            //console.log(Math.abs(delta.x),Math.abs(delta.y), decided);

            if( decided ) {
                basicEvent.preventDefault();
                basicEvent.stopPropagation();
            } else {
                if( Math.abs( delta.x ) > swipeLengthThresholdX ) {
                    basicEvent.preventDefault();
                    basicEvent.stopPropagation();
                    // if(Math.abs(delta.x) > 10 + Math.abs(delta.y)) {
                    decided = true;
                    // }
                } else if( Math.abs( delta.y ) > swipeLengthThresholdY ) {
                    //this.up();
                    return true;
                }
            }

            /*
            if( opt.continuous ) {
                if( len >= 3 ){
                    transition( circle( internalIndex - 1 ), delta.x );
                    transition( internalIndex, delta.x );
                    transition( circle( internalIndex + 1 ), delta.x );
                } else {
                    if( delta.x > 0 ){
                        repositionItem( circle( internalIndex + 1 ), -width, true );
                    } else {
                        repositionItem( circle( internalIndex + 1 ), width, true );
                    }
                    transition( internalIndex, delta.x );
                    transition( circle( internalIndex + 1 ), delta.x );
                }
            } else {
                prev = internalIndex - 1;
                next = internalIndex + 1;

                if( prev >= 0 ) {
                    transition( prev, delta.x );
                }
                transition( internalIndex, delta.x );
                if( next <= max ) {
                    transition( next, delta.x );
                }
            }
            */

            return true;
        };

        this.startEvent = function( baseEvent ){
            var event;
            baseEvent.stopPropagation();

            if( touch ) {
                event = baseEvent.originalEvent.changedTouches[ 0 ];
            } else {
                baseEvent.preventDefault();

                if( window.navigator.msPointerEnabled ) {
                    event = baseEvent.originalEvent;
                } else {
                    event = baseEvent;
                }
            }

            /*
            if( handle ) {
                clearInterval( handle );
                handle = 0;
            }
            width = container.width();
            delta = { x: 0, y: 0 };
            */
            start = {
                x: event.pageX,
                y: event.pageY
            };

            currentSlider.detectAction( event );
        };

        this.stopEvent = function( event ){
            event.preventDefault();
            event.stopPropagation();
            $tabsContainer.unbind( event );
            start.x = 0;
            this.delta.x = 0;
        };

        this.detectAction = function(){

            var _this = this;

            $tabsContainer.on( events.move, function( event ){
                // event.preventDefault();
                // event.stopPropagation();
                _this.move( event );
            }).on( events.up, function( event ){
                if( _this.delta.x === 0 && _this.delta.y === 0 && events.itemClick ){
                    console.log( 'stuff' );
                    events.itemClick.call( items.eq( currentTabIndex )[ 0 ], event );
                } else {
                    event.preventDefault();
                    event.stopPropagation();
                    _this.up( event );
                }
            });

            if( events.leave ) {
                $tabsContainer.on( events.leave, function( event ){
                    _this.stopEvent( event );

                    //_this.up(event);
                });
            }
        };

        this.up = function( event ){
            var dir = this.delta.x < 0 ? 'left' : 'right';
            var i = currentTabIndex;
            var index;

            if( Math.abs( this.delta.x ) < swipeLengthThresholdX || start.x === this.delta.x ){
                this.stopEvent( event );
            } else {
                this.stopEvent( event );

                index = i;
                if( dir === 'right' ){
                    i = i - 1;
                    index = i >= 1 ? i : items.length;
                } else if( dir === 'left' ){
                    i = i + 1;
                    index = i <= items.length ? i : 1;
                }

                this.slideTo( index );
            }

            return false;
        };

        if( 'ontouchstart' in window || window.DocumentTouch && document instanceof DocumentTouch ){
            touch = true;
        }

        $tabsContainer.find( '.tab:first' ).addClass( 'active' );

        if( touch ) {
            events.down = 'touchstart';
            events.move = 'touchmove';
            events.up = 'touchend';
        } else {
            if( window.navigator.msPointerEnabled ) {
                events.down = 'MSPointerDown';
                events.move = 'MSPointerMove';
                events.up = 'MSPointerUp';
                events.leave = 'MSPointerOut';
            } else {
                events.down = 'mousedown';
                events.move = 'mousemove';
                events.up = 'mouseup';
                events.leave = 'mouseleave';
            }
        }
        this.bindEvents();
    }

    fbinhouse.initTabs = function( selector ){
        if( typeof fbinhouse.sliders === 'undefined' ){
            fbinhouse.sliders = [];
        }

        $( selector ).each( function(){
            fbinhouse.sliders.push( new TabSlider( selector ));
        });
    };
})( jQuery );

( function( $ ){
    'use strict';

    function Gallery( selector ){
        var $images = $( selector ).find( '.large-image-wrapper' );
        var $thumbnails;
        var sliderOptions = {
            duration: 500,
            queue: false,
            easing: 'easeInOutSine'
        };
        var fadeAnim;

        $( selector ).append( '<ul class="thumbnails-nav"></div>' );
        $thumbnails = $( selector ).find( '.thumbnails-nav' );

        $.each( $images, function( index ){
            var i = index;
            var src = $( this ).find( 'img' ).attr( 'src' ).split( '.' )[ 0 ].split( '/' );
            var name = $( this ).find( 'img' ).attr( 'src' ).split( '.' )[ 0 ].split( '/' )[ 2 ];
            var path = fbinhouse.baseUrl + src[ 0 ] + '/' + src[ 1 ] + '/240/' + name + '.jpg';
            var img = '<li><a href="#" data-id="' + ( index + 1 ) + '"><img src="' + path + '" /></a></li>';

            $( this ).attr( 'data-id', i + 1 );
            $thumbnails.append( img );
        });
        $( $images[ 0 ] ).addClass( 'active' )
        $( $thumbnails.find( 'a' )[ 0 ] ).addClass( 'active' );

        fadeAnim = function( id ){
            var start = $images.filter( '[data-id="' + id + '"]' ).css( 'opacity' );
            var end = $images.filter( '[data-id="' + id + '"]' ).hasClass( 'active' ) ? 0 : 1;
            var animation = { opacity: [ end, start ] };

            return animation;
        };

        $thumbnails.find( 'a' ).on( 'click', function( e ){
            var idIn = $( this ).attr( 'data-id' );
            var idOut = $images.filter( '.active' ).attr( 'data-id' );

            e.preventDefault();

            $thumbnails.find( 'a' ).filter( '.active' ).removeClass( 'active' );
            $( this ).addClass( 'active' );

            $images.filter( '.active' ).velocity( fadeAnim( idOut ), sliderOptions ).removeClass( 'active' ).end().filter( '[data-id="' + idIn + '"]' ).velocity( fadeAnim( idIn ), sliderOptions ).addClass( 'active' );
        });

    }

    fbinhouse.initGallery = function( selector ){
        if( typeof fbinhouse.galleries === 'undefined' ){
            fbinhouse.galleries = [];
        }

        $( selector ).each( function(){
            fbinhouse.galleries.push( new Gallery( selector ));
        });
    };

})( jQuery );
